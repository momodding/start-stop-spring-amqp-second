package com.momodding.testing.configuration

import org.springframework.amqp.core.*
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class MessageQueueConfig {

    @Bean
    fun messageQueue(): Queue = QueueBuilder
        .durable("nyoba_message")
        .deadLetterExchange("nyoba_message.direct")
        .deadLetterRoutingKey("nyoba_message.dlr")
        .build()

    @Bean
    fun dlxMessageQueue(): Queue = QueueBuilder
        .durable("nyoba_message.dlx")
        .build()

    @Bean
    fun messageExchange() = DirectExchange("nyoba_message.direct", true, false)

    @Bean
    fun bindExchangeQueue(): Binding = BindingBuilder.bind(messageQueue()).to(messageExchange()).with("nyoba_message.route")

    @Bean
    fun bindExchangeDlx(): Binding = BindingBuilder.bind(dlxMessageQueue()).to(messageExchange()).with("nyoba_message.dlr")
}