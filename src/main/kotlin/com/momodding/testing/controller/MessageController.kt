package com.momodding.testing.controller

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistry
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["/message"], produces = [MediaType.APPLICATION_JSON_VALUE])
class MessageController @Autowired constructor(
    private val rabbitTemplate: RabbitTemplate,
    private val rabbitListener: RabbitListenerEndpointRegistry
) {

    @PostMapping
    fun addMessage(
        @RequestBody request: Map<String, Any>
    ): ResponseEntity<Any> {
        rabbitTemplate.
        rabbitTemplate.convertAndSend("nyoba_message", jacksonObjectMapper().writeValueAsString(request))

        return ResponseEntity(true, HttpStatus.OK)
    }

    @GetMapping("stop-listen")
    fun stopListen(): ResponseEntity<Any> = try {
            rabbitListener.getListenerContainer("messageListener").isRunning
            ResponseEntity(true, HttpStatus.OK)
        } catch (ex: Exception) {
            println(ex.localizedMessage)
            ResponseEntity(false, HttpStatus.BAD_REQUEST)
        }

    @GetMapping("start-listen")
    fun startListen(): ResponseEntity<Any> = try {
        rabbitListener.getListenerContainer("messageListener").start()
        ResponseEntity(true, HttpStatus.OK)
    } catch (ex: Exception) {
        println(ex.localizedMessage)
        ResponseEntity(false, HttpStatus.BAD_REQUEST)
    }
}