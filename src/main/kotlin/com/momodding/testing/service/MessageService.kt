package com.momodding.testing.service

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.amqp.core.Message
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class MessageService @Autowired constructor() {

    @RabbitListener(queues = ["nyoba_message"], id = "messageListener")
    fun messageListener(message: Message) {
        println(jacksonObjectMapper().writeValueAsString(message.body))
    }
}